import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

# from PyQt5.QtWidgets import *
# from PyQt5.QtGUI import *
# from PyQt5.QtCore import *

class InitialWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent


        layout = QVBoxLayout()
        bottom_box = QHBoxLayout()
        grid_edit = QGridLayout()
        
        label_title = QLabel()
        label_title.setText('<center><font size="7">Mass measurement</font></center>')

        Next_button= QPushButton("&Next")
        Next_button.clicked.connect(self.pass_result)
        Back_button= QPushButton("&Back")
        Back_button.clicked.connect(self.back_page)

        bottom_box.addWidget(Back_button)
        bottom_box.addStretch()
        bottom_box.addWidget(Next_button)


        label_text = QLabel()
        label_text.setText('weight : ')
        label_unit = QLabel()
        label_unit.setText(self.parent.test_result_dict['mass_unit'])
        label_comment = QLabel()
        label_comment.setText('comment')
        
        self.edit_comment = QLineEdit()
        self.edit_comment.setText(self.parent.test_result_dict['comment'])
        self.edit_mass = QLineEdit()
        self.edit_mass.setValidator(QIntValidator())
        if not self.parent.test_result_dict['mass_digit'] =='':
            self.edit_mass.setText(self.parent.test_result_dict['mass_digit'])
        else:
            self.edit_mass.setText('')

        grid_edit.addWidget(label_text,0,0)
        grid_edit.addWidget(self.edit_mass,0,1)
        grid_edit.addWidget(label_unit,0,2)
        grid_edit.addWidget(label_comment,1,0)
        grid_edit.addWidget(self.edit_comment,1,1)


        layout.addWidget(label_title)
        layout.addStretch()
        layout.addLayout(grid_edit)
        layout.addLayout(bottom_box)

        self.setLayout(layout)

    def pass_result(self):

        self.parent.test_result_dict['mass_digit'] = self.edit_mass.text()
        self.parent.test_result_dict['comment'] = self.edit_comment.text()        
        self.parent.return_result()

    def back_page(self):
        self.parent.close_and_return()

import os
import shutil
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import cv2
import numpy as np
import json
import pymongo
from bson.objectid import ObjectId


sys.path.append(os.path.join(os.path.dirname(__file__), '../../../dbinterface/lib'))
import localdb_uploader

sys.path.append(os.path.join(os.path.dirname(__file__), './lib'))
import initial_MASS


class MASSWindow(QMainWindow):
############################################################################################
    def __init__(self, parent=None):
#        super(QMainWindow, self).__init__(parent)
        super(QMainWindow, self).__init__()
        self.parent = parent

        self.setGeometry(0, 0, 900, 500)

        self.test_result_dict = {
            'mass_digit' :'',
            'mass_unit' :'g',
            'comment' :''
        }
############################################################################################
    def init_ui(self):
        self.initial_wid = initial_MASS.InitialWindow(self)
        self.update_widget(self.initial_wid)

    def update_widget(self, w):
        self.setCentralWidget(w)
        self.show()

    def close_and_return(self):
        self.close()
        self.parent.show()
       
    def back_page(self):
        self.parent.init_ui()

    def back_window(self):
        self.parent.recieve_backpage()
        
    def call_another_window(self,window):
        self.hide()
        window.init_ui()

    def return_result(self):
        self.parent.recieve_result(self,self.test_result_dict)
